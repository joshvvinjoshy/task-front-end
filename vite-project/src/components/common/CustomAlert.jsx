import React, { useState, useEffect } from "react";
import { Snackbar, Alert } from "@mui/material";
import "./CustomAlert.css";
export default function CustomAlert({ severity, message }) {
  const [open, setOpen] = useState(true);
  useEffect(() => {
    const timer = setTimeout(() => {
      handleClose();
    }, 2000);

    return () => {
      clearTimeout(timer);
    };
  }, []);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Snackbar
      className="alert-container"
      open={open}
      onClose={handleClose}
      autoHideDuration={2000}
    >
      <Alert
        className="alert"
        variant="filled"
        severity={severity}
        onClose={handleClose}
      >
        {message}
      </Alert>
    </Snackbar>
  );
}
