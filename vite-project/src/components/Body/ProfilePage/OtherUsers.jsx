import React, { useState } from "react";
import "./OtherUsers.css";
import { TableCell } from "@mui/material";
import CustomAlert from "../../common/CustomAlert";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import EditUser from "./EditUser";
export default function OtherUsers({ user, index }) {
  const [userDetails, setUserDetails] = useState(user);
  const [formSubmitted, setFormSubmitted] = useState(false);

  return (
    <>
      {formSubmitted ? (
        <CustomAlert severity="success" message="User updated Successfully" />
      ) : null}
      <TableCell className="headers userContents">{index + 1}</TableCell>
      <TableCell className="headers userContents">
        {userDetails.username}
      </TableCell>
      <TableCell className="headers userContents">
        {userDetails.emailaddress}
      </TableCell>
      <TableCell className="headers userContents">
        {userDetails.gender}
      </TableCell>
      <TableCell className="headers userContents">
        {moment(userDetails.dateofbirth).format("DD/MM/YYYY")}
      </TableCell>
      <TableCell className="headers userContents">
        <EditUser
          userDetails={userDetails}
          setUserDetails={setUserDetails}
          setFormSubmitted={setFormSubmitted}
        />
      </TableCell>
    </>
  );
}
