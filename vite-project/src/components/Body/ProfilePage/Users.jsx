import React, { useState, useEffect } from "react";
import OtherUsers from "./OtherUsers";
import { TableRow } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import moment from "moment";
import axios from "axios";
export default function Users({ username, csv_data, setNoCheckedUsers }) {
  const [users, setUsers] = useState([]);
  const [checkedUsers, setCheckedUsers] = useState({});
  useEffect(() => {
    const url = `${import.meta.env.VITE_URL}/users`;
    axios.get(url).then((res) => {
      setUsers(res.data);
    });
  }, []);
  function handleCheckChange(event, user) {
    setCheckedUsers((oldCheckedUsers) => ({
      ...oldCheckedUsers,
      [user.username]: event.target.checked,
    }));
  }
  useEffect(() => {
    const newCheckedUsers = Object.keys(checkedUsers).some((user) => {
      return checkedUsers[user];
    });
    setNoCheckedUsers(!newCheckedUsers);
  }, [checkedUsers]);
  return (
    <>
      {users
        .filter((user) => user.username != username)
        .map((user, index) => {
          const userName = user.username;
          if (checkedUsers[userName] != undefined && checkedUsers[userName]) {
            const userlist = [
              user.username,
              user.emailaddress,
              user.gender,
              moment(user.dateofbirth).format("DD/MM/YYYY"),
            ];
            csv_data.push(userlist);
          }
          return (
            <TableRow key={user.username}>
              <OtherUsers user={user} index={index} key={user.username} />
              <Checkbox
                className="checkbox"
                sx={{ marginTop: "15px" }}
                color="success"
                onChange={(event) => handleCheckChange(event, user)}
              />
            </TableRow>
          );
        })}
    </>
  );
}
