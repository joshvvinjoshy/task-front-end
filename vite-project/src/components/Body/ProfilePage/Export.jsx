import React, { useRef } from "react";
import { CSVLink } from "react-csv";
import { Button } from "@mui/material";
export default function Export({
  noCheckedUsers,
  setNoCheckedUsers,
  csv_data,
}) {
  const csv = useRef();

  function handleExportClick() {
    csv.current.link.click();
    setNoCheckedUsers(false);
  }
  return (
    <>
      <Button
        variant="contained"
        className="buttons"
        color="success" 
        onClick={handleExportClick}
        disabled={noCheckedUsers ? true : false}
      >
        Export
      </Button>
      <CSVLink
        className="downloadbtn"
        filename="users.csv"
        data={csv_data}
        ref={csv}
      >
        Export to CSV
      </CSVLink>
    </>
  );
}
