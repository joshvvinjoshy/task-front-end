import React, { useState, useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import "./ProfilePage.css";
import CustomAlert from "../../common/CustomAlert";
import Export from "./Export";
import Import from "./Import";
import Users from "./Users";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Pagination,
} from "@mui/material";
const style = {
  color: "white",
  fontSize: "18px",
};
export default function ProfilePage({ loggedin, setCookie }) {
  const { username } = useParams();
  const [noCheckedUsers, setNoCheckedUsers] = useState(true);
  const [uploadSuccessful, setUploadSuccessful] = useState(false);
  const csv_data = [["Username", "Email", "Gender", "Date of Birth"]];

  function handleLogout() {
    setCookie("loggedin", false);
    navigate("/");
  }
  const navigate = useNavigate();
  return (
    <div className="profile-container" key={username}>
      {loggedin ? (
        <CustomAlert severity="success" message="Successfully Logged In" />
      ) : null}
      {uploadSuccessful ? (
        <CustomAlert severity="success" message="File Uploaded Successfully" />
      ) : null}
      <div className="profile-heading-container">
        <Typography variant="h4" className="profile-heading">
          Welcome {username}
        </Typography>
        <Button
          variant="contained"
          sx={{ backgroundColor: "red" }}
          onClick={handleLogout}
        >
          Logout
        </Button>
      </div>
      <TableContainer component={Paper} className="profile-details">
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow className="headers-container">
              <TableCell className="headers" sx={{ ...style }}>
                Id
              </TableCell>
              <TableCell className="headers" sx={{ ...style }}>
                Username
              </TableCell>
              <TableCell className="headers" sx={{ ...style }}>
                Email
              </TableCell>
              <TableCell className="headers" sx={{ ...style }}>
                Gender
              </TableCell>
              <TableCell className="headers" sx={{ ...style }}>
                Date of Birth
              </TableCell>
              <TableCell className="headers" sx={{ ...style }}>
                Edit
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody className="contents">
            <Users
              username={username}
              csv_data={csv_data}
              setNoCheckedUsers={setNoCheckedUsers}
            />
          </TableBody>
        </Table>
      </TableContainer>
      <div className="pagination-container">
        <Pagination
          className="pagination"
          count={10}
          color="success"
          size="large"
        />
        <div className="import-export">
          <Export
            noCheckedUsers={noCheckedUsers}
            setNoCheckedUsers={setNoCheckedUsers}
            csv_data={csv_data}
          />
          <Import setUploadSuccessful={setUploadSuccessful} />
        </div>
      </div>
    </div>
  );
}
