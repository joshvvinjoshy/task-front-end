import React, { useState } from "react";
import DatePicker from "react-datepicker";
import {
  Button,
  Popover,
  Typography,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
} from "@mui/material";
import axios from "axios";
import moment from "moment";
export default function EditUser({
  userDetails,
  setUserDetails,
  setFormSubmitted,
}) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const validdates = (date) => moment().subtract(16, "years") > date;

  const [formData, setFormData] = useState({
    gender: userDetails.gender,
    dateofbirth: new Date(userDetails.dateofbirth),
  });
  const handleGenderChange = (event) => {
    setFormData({ ...formData, gender: event.target.value });
  };
  const handleDateChange = (date) => {
    setFormData({
      ...formData,
      dateofbirth: date,
    });
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleFormSubmit = (event) => {
    event.preventDefault();
    setFormSubmitted(true);
    setUserDetails((oldUserDetails) => ({
      ...oldUserDetails,
      gender: formData.gender,
      dateofbirth: formData.dateofbirth,
    }));
    const api = `${import.meta.env.VITE_URL}/users/${user.username}`;
    axios
      .put(api, formData)
      .then((res) => {
        setFormSubmitted(true);
      })
      .catch((err) => {
        console.error(err);
      });
    handleClose();
  };
  return (
    <>
      <Button variant="contained" onClick={handleClick} color="success">
        Edit
      </Button>
      <Popover
        open={open}
        onClose={handleClose}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <form onSubmit={handleFormSubmit} method="POST">
          <div className="edit-container">
            <Typography variant="h5" className="form-name">
              Edit Details
            </Typography>
            <div className="form-contents">
              <Typography className="date-name" style={{ marginLeft: "-20px" }}>
                Date of Birth
              </Typography>
              <DatePicker
                className="form-date"
                filterDate={validdates}
                selected={new Date(formData.dateofbirth)}
                onChange={handleDateChange}
                dateFormat="dd-MM-yyyy"
                value={new Date(formData.dateofbirth)}
                required
              />
            </div>
            <FormControl fullWidth className="textfields">
              <InputLabel id="demo-simple-select-label">Gender</InputLabel>
              <Select
                value={formData.gender}
                label="Gender"
                onChange={handleGenderChange}
                required
              >
                <MenuItem value={"Male"}>Male</MenuItem>
                <MenuItem value={"Female"}>Female</MenuItem>
              </Select>
            </FormControl>
            <Button
              variant="contained"
              className="editSubmit"
              type="submit"
              color="success"
              disabled={
                formData.gender == "" || formData.dateofbirth == null
                  ? true
                  : false
              }
            >
              Submit
            </Button>
          </div>
        </form>
      </Popover>
    </>
  );
}
