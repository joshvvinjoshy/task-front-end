import React, { useState } from "react";
import axios from "axios";
import { Button, Modal, Typography, Box } from "@mui/material";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};
export default function Import({ setUploadSuccessful }) {
  const [file, setFile] = useState(null);
  const [open, setOpen] = useState(false);

  function handleFormSubmit(event) {
    event.preventDefault();
    const formData = new FormData();
    formData.append("file", file);
    formData.append("filename", file.name);
    const url = `${import.meta.env.VITE_URL}/uploadfile`;
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };
    axios
      .post(url, formData, config)
      .then((response) => {
        handleClose();
        setUploadSuccessful(true);
        console.log(response.data);
      })
      .catch((err) => {
        console.error(err);
      });
  }
  function handleImportClick(event) {
    setOpen(true);
  }
  function handleClose() {
    setOpen(false);
  }
  function handleFileChange(event) {
    setFile(event.target.files[0]);
  }
  return (
    <>
      <Button
        variant="contained"
        className="buttons"
        color="success"
        onClick={handleImportClick}
      >
        Import
      </Button>
      <Modal open={open} onClose={handleClose}>
        <Box sx={{ ...style }}>
          <form
            method="POST"
            onSubmit={handleFormSubmit}
            className="import-popover"
          >
            <Typography className="upload-contents upload-title" variant="h6">
              Upload File
            </Typography>
            <div className="upload-contents upload-main">
              <Typography className="upload-label">
                Select File to Upload
              </Typography>
              <input
                className="upload-file"
                type="file"
                accept=".csv"
                onChange={handleFileChange}
              />
            </div>
            <Button
              className="upload-contents upload-button"
              variant="contained"
              color="success"
              type="submit"
              disabled={file ? false : true}
            >
              Upload
            </Button>
          </form>
        </Box>
      </Modal>
    </>
  );
}
