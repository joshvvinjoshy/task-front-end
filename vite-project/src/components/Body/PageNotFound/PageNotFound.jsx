import React from "react";
import { Typography } from "@mui/material";
export default function PageNotFound() {
  return (
    <div className="page-not-found">
      <Typography variant="h5">Page Not Found</Typography>
    </div>
  );
}
