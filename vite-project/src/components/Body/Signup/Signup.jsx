import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import CustomAlert from "../../common/CustomAlert";
import axios from "axios";
import moment from "moment";
import validator from "validator";
import "./Signup.css";
import signupimage from "../../../assets/varthana-login.webp";

export default function Signup() {
  const [formDetails, setFormDetails] = useState({
    username: "",
    emailaddress: "",
    password: "",
    gender: "Male",
    dateofbirth: moment(new Date("1999-10-25")),
  });
  const [alert, setAlert] = useState({
    name: "",
    message: "",
  });
  const navigate = useNavigate();
  function handleFormSubmit(event) {
    event.preventDefault();
    if (
      formDetails.username == "" ||
      formDetails.password == "" ||
      formDetails.emailaddress == ""
    ) {
      setAlert({
        name: "error",
        message: "Please Enter all the Mandatory Details",
      });
    } else {
      if (validator.isEmail(formDetails.emailaddress)) {
        const url = `${import.meta.env.VITE_URL}/signup`;
        axios
          .post(url, formDetails)
          .then((res) => {
            setAlert({
              name: "success",
              message: "User Account Created Successfully",
            });
            setTimeout(() => {
              navigate("/");
            }, 1000);
          })
          .catch((err) => {
            setAlert({
              name: "error",
              message: err.response.data,
            });
            setTimeout(() => {
              setAlert({ name: "", message: "" });
            }, 1500);
          });
      } else {
        setAlert({ name: "error", message: "Enter a Valid Email Address" });
        setTimeout(() => {
          setAlert({ name: "", message: "" });
        }, 1500);
      }
    }
  }
  function handleUserChange(event) {
    setFormDetails({ ...formDetails, username: event.target.value });
  }
  function handlePasswordChange(event) {
    setFormDetails({ ...formDetails, password: event.target.value });
  }
  function handleEmailChange(event) {
    setFormDetails({ ...formDetails, emailaddress: event.target.value });
  }
  return (
    <div className="signup-c">
      <div className="signup-image-container">
        <img src={signupimage} className="signup-image" alt="signup-image" />
      </div>
      <div className="signup-container">
        <form onSubmit={handleFormSubmit} method="POST" className="signup">
          {alert.name ? (
            <CustomAlert severity={alert.name} message={alert.message} />
          ) : null}
          <Typography variant="h5" id="signup-text">
            {" "}
            Signup{" "}
          </Typography>
          <TextField
            id="username"
            className="textfields"
            label="Username"
            variant="outlined"
            onChange={handleUserChange}
          />
          <TextField
            id="email"
            className="textfields"
            label="Email"
            variant="outlined"
            type="text"
            onChange={handleEmailChange}
          />
          <TextField
            id="password"
            className="textfields"
            label="Password"
            variant="outlined"
            type="password"
            onChange={handlePasswordChange}
          />
          <Button variant="contained" id="submit" type="submit">
            {" "}
            create account
          </Button>
          <div className="goto-login">
            <Button
              variant="text"
              className="login-button"
              onClick={() => navigate("/")}
            >
              SIGNIN
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
