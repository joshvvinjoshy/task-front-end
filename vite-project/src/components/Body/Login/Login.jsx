import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import CustomAlert from "../../common/CustomAlert";
import axios from "axios";
import loginimage from "../../../assets/varthana-login.webp";
export default function Login({ setCookie }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [alert, setAlert] = useState("");
  const navigate = useNavigate();
  function handleSubmitClick(event) {
    if (!username || !password) {
      setAlert("Please Enter Username and Password");
      setTimeout(() => {
        setAlert("");
      }, 1500);
    } else {
      const url = `${import.meta.env.VITE_URL}/login`;
      const data = {
        username: username,
        password: password,
      };
      axios
        .post(url, data)
        .then((res) => {
          setCookie("loggedin", true);
          navigate(`/${username}`);
        })
        .catch((err) => {
          setAlert("Authentication Failed");
          setTimeout(() => {
            setAlert("");
          }, 1500);
          console.error(err);
        });
    }
  }
  function handleUserChange(event) {
    setUsername(event.target.value);
  }
  function handlePasswordChange(event) {
    setPassword(event.target.value);
  }
  return (
    <div className="login-container">
      <div className="login-image-container">
        <img src={loginimage} className="login-image" alt="login-image" />
      </div>
      <div className="login-details">
        <div className="login-contents">
          {alert ? <CustomAlert severity="error" message={alert} /> : null}

          <Typography variant="h5" id="login-text">
            {" "}
            Login{" "}
          </Typography>
          <TextField
            id="username"
            className="textfields"
            label="Username"
            variant="outlined"
            onChange={handleUserChange}
          />
          <TextField
            id="password"
            className="textfields"
            label="Password"
            variant="outlined"
            type="password"
            onChange={handlePasswordChange}
          />
          <Button
            variant="contained"
            id="submit"
            style={{ height: "8%" }}
            onClick={handleSubmitClick}
          >
            {" "}
            signin
          </Button>
          <div className="goto-signup">
            <Button
              variant="text"
              className="signup-button"
              onClick={() => navigate("/signup")}
            >
              create account
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
