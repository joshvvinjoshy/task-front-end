import React from "react";
import { AppBar, Toolbar } from "@mui/material";
import varthana from "../../assets/varthana.png";

import "./Header.css";
export default function Header() {
  return (
    <div className="header">
      <AppBar position="static">
        {" "}
        <Toolbar sx={{ backgroundColor: "white" }}>
          <div className="img-container">
            <img className="img" src={varthana} alt="varthana" />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
