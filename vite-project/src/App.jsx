import React, { useState } from "react";
import Header from "./components/Header/Header";
import "./App.css";
import Login from "./components/Body/Login/Login";
import Signup from "./components/Body/Signup/Signup";
import ProfilePage from "./components/Body/ProfilePage/ProfilePage";
import PageNotFound from "./components/Body/PageNotFound/PageNotFound";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useCookies } from "react-cookie";
function App() {
  const [cookies, setCookie] = useCookies(["loggedin"]);
  return (
    <div className="body">
      <Header />
      <div className="body-container">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Login setCookie={setCookie} />}></Route>
            <Route path="/signup" element={<Signup />}></Route>
            {cookies.loggedin ? (
              <Route
                path="/:username"
                element={
                  <ProfilePage
                    loggedin={cookies.loggedin}
                    setCookie={setCookie}
                  />
                }
              ></Route>
            ) : null}
            <Route path="*" element={<PageNotFound />}></Route>
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
